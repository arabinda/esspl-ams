# ESSPL Asset Management System Sample Business Network

> ESSPL Asset Management System demonstrates the core functionality of Hyperledger Composer by doing multiple transaction over asset.

This business network defines:

**Participant**
`Employee`

**Asset**
`EssplAsset`

**Transaction**
`UpdateAsset`
`AssignAssetToEmployee`

**Event**
`SampleEvent`

EssplAsset are owned by an Employee, and the value property on a Asset can be modified by submitting a UpdateAsset. The UpdateAsset emits a SampleEvent that notifies applications of the old and new values for each modified Asset.

To test this Business Network Definition in the **Test** tab:

Create a `SampleParticipant` participant:

```
{
  "$class": "rg.esspl.ams.Employee",
  "participantId": "Toby",
  "firstName": "Tobias",
  "lastName": "Hunter"
}
```

Create a `SampleAsset` asset:

```
{
  "$class": "org.acme.sample.SampleAsset",
  "assetId": "assetId:1",
  "owner": "resource:org.acme.sample.SampleParticipant#Toby",
  "value": "original value"
}
```

Submit a `SampleTransaction` transaction:

```
{
  "$class": "org.acme.sample.SampleTransaction",
  "asset": "resource:org.acme.sample.SampleAsset#assetId:1",
  "newValue": "new value"
}
```

After submitting this transaction, you should now see the transaction in the Transaction Registry and that a `SampleEvent` has been emitted. As a result, the value of the `assetId:1` should now be `new value` in the Asset Registry.

Congratulations!
