/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Sample transaction processor function.
 * @param {org.esspl.ams.UpdateAsset} tx The sample transaction instance.
 * @transaction
 */
function UpdateAsset(tx) {

    // Save the old value of the asset.
    var oldAssetType = tx.asset.assetType;
    var oldAssetName = tx.asset.assetName;

    // Update the asset with the new value.
    tx.asset.assetType = tx.newAssetType;
    tx.asset.assetName = tx.newAssetName;

    // Get the asset registry for the asset.
    return getAssetRegistry('org.esspl.ams.CommonAsset')
        .then(function (assetRegistry) {

            // Update the asset in the asset registry.
            return assetRegistry.update(tx.asset);

        })
        .then(function () {

            // Emit an event for the modified asset.
            var event = getFactory().newEvent('org.esspl.ams', 'SampleEvent');
            event.asset = tx.asset;
            event.oldValue = oldAssetType;
            event.newValue = tx.asset.assetType;
            emit(event);

        });

}

/**
 * Sample transaction processor function.
 * @param {org.esspl.ams.AssignAssetToEmployee} tx Assign asset to an Employee.
 * @transaction
 */
function AssignAssetToEmployee(tx) {  
    
    var owner = tx.asset.owner;
    var oldOwner = "N/A";
   	if(owner)
      oldOwner = owner.empCode;    
	tx.asset.owner = tx.participant;
   
    // Get the asset registry for the asset.
    return getAssetRegistry('org.esspl.ams.CommonAsset')
        .then(function (assetRegistry) {
			
            // update the asset into asset registory
            return assetRegistry.update(tx.asset);

        })
        .then(function () {

            // Emit an event for the modified asset.
            var event = getFactory().newEvent('org.esspl.ams', 'SampleEvent');
            event.asset = tx.asset;
            event.oldValue = oldOwner;
            event.newValue = tx.asset.owner.empCode;
            emit(event);

        });

}