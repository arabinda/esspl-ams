echo "stopping the rest server"
kill $(ps aux | grep composer-rest-[server] | awk '{print $2}')
echo "stopped the rest server"
sleep 10
echo "starting the rest server"

composer-rest-server -p hlfv1 -n esspl-ams -i admin -s adminpw -N always -w true &
