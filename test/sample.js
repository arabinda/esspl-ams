/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const AdminConnection = require('composer-admin').AdminConnection;
const BrowserFS = require('browserfs/dist/node/index');
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const BusinessNetworkDefinition = require('composer-common').BusinessNetworkDefinition;
const path = require('path');

const chai = require('chai');
chai.should();
chai.use(require('chai-as-promised'));

const bfs_fs = BrowserFS.BFSRequire('fs');

describe('Sample', () => {

    // This is the business network connection the tests will use.
    let businessNetworkConnection;

    // This is the factory for creating instances of types.
    let factory;

    // These are the identities for Alice and Bob.
    let aliceIdentity;
    let bobIdentity;

    // These are a list of receieved events.
    let events;

    // This is called before each test is executed.
    beforeEach(() => {

        // Initialize an in-memory file system, so we do not write any files to the actual file system.
        BrowserFS.initialize(new BrowserFS.FileSystem.InMemory());

        // Create a new admin connection.
        const adminConnection = new AdminConnection({ fs: bfs_fs });

        // Create a new connection profile that uses the embedded (in-memory) runtime.
        return adminConnection.createProfile('defaultProfile', { type : 'embedded' })
            .then(() => {

                // Establish an admin connection. The user ID must be admin. The user secret is
                // ignored, but only when the tests are executed using the embedded (in-memory)
                // runtime.
                return adminConnection.connect('defaultProfile', 'admin', 'adminpw');

            })
            .then(() => {

                // Generate a business network definition from the project directory.
                return BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));

            })
            .then((businessNetworkDefinition) => {

                // Deploy and start the business network defined by the business network definition.
                return adminConnection.deploy(businessNetworkDefinition);

            })
            .then(() => {

                // Create and establish a business network connection
                businessNetworkConnection = new BusinessNetworkConnection({ fs: bfs_fs });
                events = [];
                businessNetworkConnection.on('event', (event) => {
                    events.push(event);
                });
                return businessNetworkConnection.connect('defaultProfile', 'esspl-ams', 'admin', 'adminpw');

            })
            .then(() => {

                // Get the factory for the business network.
                factory = businessNetworkConnection.getBusinessNetwork().getFactory();

                // Create the participants.
                const arabinda1 = factory.newResource('org.esspl.ams', 'Employee', 'SD704');
                arabinda1.firstName = 'Arabinda1';
                arabinda1.lastName = 'Nanda';
                arabinda1.email="arabinda1@esspl.com";
                const arabinda2 = factory.newResource('org.esspl.ams', 'Employee', 'SD705');
                arabinda2.firstName = 'Arabinda2';
                arabinda2.lastName = 'Nanda';
                arabinda2.email="arabinda2@esspl.com";
                return businessNetworkConnection.getParticipantRegistry('org.esspl.ams.Employee')
                    .then((participantRegistry) => {
                        participantRegistry.addAll([arabinda1, arabinda2]);
                    });

            })
            .then(() => {

                // Create the assets.
                const asset1 = factory.newResource('org.esspl.ams', 'CommonAsset', 'asset1');
                asset1.owner = factory.newRelationship('org.esspl.ams', 'Employee', 'SD704');
                asset1.assetType = 'Laptop';
                asset1.assetName = "Dell Vostro";
                const asset2 = factory.newResource('org.esspl.ams', 'CommonAsset', 'asset2');
                asset2.owner = factory.newRelationship('org.esspl.ams', 'Employee', 'SD705');
                asset2.assetType = 'Laptop';
                asset2.assetName = "HP";
                return businessNetworkConnection.getAssetRegistry('org.esspl.ams')
                    .then((assetRegistry) => {
                        assetRegistry.addAll([asset1, asset2]);
                    });
            })
            .then(() => {

                // Issue the identities.
                return businessNetworkConnection.issueIdentity('org.esspl.ams.Employee#SD704', 'arabinda1')
                    .then((identity) => {
                        aliceIdentity = identity;
                        return businessNetworkConnection.issueIdentity('org.esspl.ams.Employee#SD705', 'arabinda2');
                    })
                    .then((identity) => {
                        bobIdentity = identity;
                    });

            });

    });
    
});
